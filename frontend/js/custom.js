jQuery(document).ready(function(){
     

    jQuery(document).click(function(e) {
    var target = e.target;

    if (!jQuery(target).is('.js--dropdown__content') && !jQuery(target).parents().is('.js--dropdown__content')) {
        jQuery('.js--dropdown__content').hide();
    }

    if (!jQuery(target).is('.js--header__nav__list') && !jQuery(target).parents().is('.js--header__nav__list')) {
        jQuery('.js--header__nav__list').hide();
        }
    })



    window.onscroll = function() {

    	if (window.pageYOffset >= 50){
            jQuery('.header__nav').addClass("prefixed-header");     
        }

        else {
            jQuery('.header__nav').removeClass("prefixed-header");
        }

        if (window.pageYOffset >= 150){
            jQuery('.header__nav').addClass("fixed");     
        }
        else {
            jQuery('.header__nav').removeClass("fixed");
        }
    }

    //Mobile Nav

    jQuery('.js--nav-icon').click(function(e) {
        e.stopPropagation();       
        var nav = jQuery('.js--header__nav__list');
        nav.slideToggle(200);
    });

    jQuery('.js--dropdown').click(function(e) {
        e.preventDefault();
        e.stopPropagation();

        var dropdown = jQuery('.js--dropdown__content');
        dropdown.slideToggle(200);
    });

})    
