<?php
/**
 * Created by PhpStorm.
 * User: JohnRich
 * Date: 1/27/2018
 * Time: 1:24 PM
 */

$path = trim(substr(__DIR__, strlen($_SERVER['DOCUMENT_ROOT'])), '\/');

header('Location: /' . $path . '/public');
exit;

?>
