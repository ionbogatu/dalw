*,
*::after,
*::before {
  margin: 0;
  padding: 0;
  box-sizing: inherit; }

html {
  font-size: 62.5%; }

body {
  box-sizing: border-box;
  font-family: "Open Sans", sans-serif;
  font-size: 1.6rem; }

.section-about__heading {
  font-size: 4.2rem;
  font-weight: 300; }

.section-about__subheading {
  font-size: 1.6rem;
  font-weight: 700; }

.section-about__text {
  font-size: 1.6rem; }

.section-events__heading {
  font-size: 2.8rem; }

.section-events__single__details__text {
  font-size: 1.8rem; }

.section-events .btn__link {
  font-size: 1.4rem; }

.section-contact__heading {
  font-size: 2rem;
  text-transform: capitalize;
  font-family: "Opens Sans", sans-serif; }

.section-contact__text {
  font-size: 1.4rem;
  font-family: "Opens Sans", sans-serif;
  line-height: 2.5rem; }

.footer__copyright__text {
  font-size: 2rem;
  font-weight: 700; }

.footer__copyright__text, .footer__copyright__owner {
  color: #777777; }

.prefixed-header {
  position: fixed;
  top: 0;
  left: 0;
  transform: translateY(-100%); }

.fixed {
  background-color: #111111;
  width: 100%;
  z-index: 99;
  transform: translateY(0);
  position: fixed;
  top: 0;
  left: 0;
  transition: all .3s ease-out; }

.btn {
  font-size: 2rem;
  color: #ffffff;
  background-color: #004c70;
  line-height: 2rem;
  border: none;
  border-radius: 2.5rem; }
  .btn__link:link, .btn__link:visited {
    display: inline-block;
    width: 100%;
    height: 100%;
    padding: 1.2rem 3.5rem;
    color: #ffffff;
    text-decoration: none;
    border-radius: 2.5rem;
    transition: all .2s; }
  .btn__link:hover, .btn__link:focus {
    background-color: #ffffff;
    color: #004c70; }

.logo-box {
  margin-right: auto; }
  .logo-box img {
    width: 10rem;
    object-fit: cover; }

.footer {
  padding: 1.5rem 5rem;
  display: flex;
  justify-content: space-between; }
  .footer__copyright {
    display: flex;
    flex-direction: column;
    justify-content: center; }
    .footer__copyright__text, .footer__copyright__owner {
      text-align: right; }

.row {
  max-width: 95vw;
  margin: 0 auto; }
  .row:not(:last-child) {
    margin-bottom: 3rem; }
  .row [class^="col-"] {
    float: left; }
    .row [class^="col-"]:not(:last-child) {
      margin-right: 3rem; }
  .row .col-1-of-2 {
    width: calc((100% - 3rem) / 2); }
  .row .col-1-of-3 {
    width: calc((100% - 2 * 3rem) / 3); }
  .row .col-1-of-4 {
    width: calc((100% - 3 * 3rem) / 4); }
  .row .col-2-of-3 {
    width: calc(2 * ((100% - 2 * 3rem) / 3) + 3rem); }
  .row .col-2-of-4 {
    width: calc(2 * ((100% - 3 * 3rem) / 4) + 3rem); }
  .row .col-3-of-4 {
    width: calc(3 * ((100% - 3 * 3rem) / 4) +  2 * 3rem); }

.header {
  height: 100vh;
  background-image: url("../img/background-header.jpg");
  background-size: cover;
  background-position: right;
  position: relative; }
  .header__nav {
    display: flex;
    align-items: center;
    background-color: #111111;
    padding: 1.5rem 5rem; }
    .header__nav__list {
      list-style: none; }
    .header__nav__item {
      display: inline-block; }
    .header__nav__link:link, .header__nav__link:visited {
      margin: .5rem 1rem;
      padding-bottom: .3rem;
      text-decoration: none;
      text-transform: uppercase;
      font-size: 1.6rem;
      color: #ffffff;
      border-bottom: 2px solid transparent;
      transition: all .2s;
      transform-origin: center;
      position: relative; }
      .header__nav__link:link .underline, .header__nav__link:visited .underline {
        position: absolute;
        bottom: -3px;
        left: 0;
        right: 0;
        width: 100%;
        height: 2px;
        transform: scaleX(0);
        display: inline-block;
        border-bottom: 2px solid #E5097F;
        transform-origin: center;
        transition: all .2s; }
    .header__nav__link:hover, .header__nav__link:active, .header__nav__link:focus {
      color: #E5097F; }
      .header__nav__link:hover .underline, .header__nav__link:active .underline, .header__nav__link:focus .underline {
        transform: scaleX(1); }
  .header .btn {
    position: absolute;
    top: 80%;
    left: 50%;
    transform: translate(-50%, -50%); }

.section-about {
  padding: 5rem;
  background-color: #ffffff;
  display: flex;
  flex-direction: column;
  align-items: center; }
  .section-about__heading {
    color: #004c70;
    text-transform: uppercase;
    margin-bottom: 1.5rem; }
  .section-about__subheading {
    color: #000000;
    margin-bottom: 3.5rem; }
  .section-about__text {
    font-family: 'Open Sans', sans-serif;
    margin-bottom: 2rem;
    text-align: center;
    text-align: justify;
    max-width: 56rem; }
    .section-about__text--link:link, .section-about__text--link:visited {
      text-decoration: none;
      font-weight: 700;
      color: #004c70;
      transition: all .2s; }
    .section-about__text--link:hover, .section-about__text--link:focus {
      color: #0079b4; }
  .section-about__text:last-of-type {
    margin-bottom: 3.5rem; }

.section-contact {
  padding: 5rem;
  background-color: #000000; }
  .section-contact__wrap {
    max-width: 56rem;
    margin: 0 auto;
    display: flex;
    flex-direction: column;
    align-items: center; }
  .section-contact__heading {
    color: #ffffff;
    margin-bottom: 2.5rem; }
  .section-contact .logo-contact {
    max-width: 100%; }
  .section-contact__text {
    margin: 2.5rem 0;
    color: #ffffff;
    text-align: center; }
  .section-contact__list {
    list-style: none;
    display: flex;
    flex-direction: column;
    align-items: flex-start; }
  .section-contact__item:not(:last-of-type) {
    margin: 0 auto 2.5rem auto; }
  .section-contact__link:link, .section-contact__link:visited {
    text-decoration: none;
    color: #ffffff;
    transition: all .2s; }
  .section-contact__link:hover, .section-contact__link:focus {
    color: #004c70; }

.section-events {
  padding: 5rem 0; }
  .section-events__heading {
    padding: 2rem 0;
    color: #ffffff;
    text-align: center;
    text-transform: uppercase;
    background-color: #004c70; }
  .section-events__single {
    display: flex;
    justify-content: center;
    padding: 5rem; }
    .section-events__single__poster {
      margin-right: 1.5rem;
      flex: 1 1 50%;
      display: flex;
      justify-content: flex-end; }
      .section-events__single__poster figure {
        height: 56rem;
        width: 56rem; }
        .section-events__single__poster figure img {
          width: 100%;
          height: 100%;
          object-fit: contain; }
    .section-events__single__details {
      margin-left: 1.5rem;
      padding-right: 15%;
      flex: 1 1 50%;
      display: flex;
      flex-direction: column;
      justify-content: center; }
      .section-events__single__details__text {
        display: flex;
        justify-content: flex-start;
        margin-bottom: 2rem;
        text-align: justify; }
  .section-events .btn {
    margin: 0 auto;
    display: table; }
    .section-events .btn__link {
      text-transform: uppercase; }
    .section-events .btn__link:hover {
      background-color: #0079b4;
      color: #ffffff; }
