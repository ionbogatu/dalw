(function($){

    /* var height = $('#tutorial-content .video').outerHeight();

    $('#tutorial-content .right-bar, #tutorial-content .video').css({
        'height': height
    }); */

    var selectTutorial = function(tutorial){
        var id = tutorial.data('id');
        var videoHeight = $('#tutorial-content .video .lesson[data-id="' + id + '"]').outerHeight(true);

        var offset = 0;
        $('#tutorial-content .video .lesson').each(function(){
            if($(this).data('id') === id){
                return false;
            }
            offset += $(this).outerHeight();
        });

        var rightBarHeight = $('#tutorial-content .right-bar .contents').outerHeight(true);
        $('#tutorial-content .right-bar, #tutorial-content .video').css({
            'height': (videoHeight > rightBarHeight) ? videoHeight : rightBarHeight,
            'top': -offset
        });
    };

    $(document).ready(function(){
        selectTutorial($('#tutorial-content .video .lesson[data-id="0"]'));

        $('#tutorial-content .right-bar .content-item').click(function(){
            selectTutorial($(this));
        });
    });

    setInterval(function(){
        for(var key in window.tutorials){
            var i = 0;
            for(var j in window.tutorials[key].steps){
                setTimeout(function(){
                    $('.steps-container .left-footstep').css({'transform': window.tutorials[key].steps[j].leftFoot.transform});
                }, i * parseInt(window.tutorials[key].timming));
                setTimeout(function(){
                    $('.steps-container .right-footstep').css({'transform': window.tutorials[key].steps[j].rightFoot.transform});
                }, i * parseInt(window.tutorials[key].timming));

                i++;
            }
        }
    }, 1000);
})(jQuery);