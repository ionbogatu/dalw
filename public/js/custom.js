jQuery(document).ready(function(){
     
    window.onscroll = function() {

    	if (window.pageYOffset >= 50){
            jQuery('.header__nav').addClass("prefixed-header");     
        }

        else {
            jQuery('.header__nav').removeClass("prefixed-header");
        }

        if (window.pageYOffset >= 150){
            jQuery('.header__nav').addClass("fixed");     
        }
        else {
            jQuery('.header__nav').removeClass("fixed");
        }
    }
})    
