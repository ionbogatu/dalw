<?php
/**
 * Created by PhpStorm.
 * User: JohnRich
 * Date: 1/27/2018
 * Time: 1:13 PM
 */

class Home extends Controller
{
    public function index()
    {
        $this->view('home/index');
    }
}