<?php
/**
 * Created by PhpStorm.
 * User: Florentin
 * Date: 1/29/2018
 * Time: 2:16 PM
 */

class e404 extends Controller
{
    public function index()
    {
        $this->view('e404/index');
    }
}