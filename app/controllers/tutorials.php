<?php
/**
 * Created by PhpStorm.
 * User: JohnRich
 * Date: 1/27/2018
 * Time: 1:13 PM
 */

class Tutorials extends Controller
{
    public function show($id)
    {
        $data = [];

        if(!empty($id)){
            $data['id'] = $id;
        }

        $this->view('tutorials/show', $data);
    }

    public function index()
    {
        $this->view('tutorials/show');
    }
}