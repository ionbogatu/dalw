<?php
/** @var \PathBuilder $pathBuilder */
$pathBuilder = $this->helper('PathBuilder');
$viewTemplatesPath = $pathBuilder->getViewTemplatesPath();
include $viewTemplatesPath . 'header.php';
?>
<section class="section-error">
    <h2 class="section-error__error">404: Not Found</h2>
</section>

<?php include $viewTemplatesPath . 'footer.php'; ?>
