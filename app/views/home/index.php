<?php
    /** @var \Home $this*/
    $viewTemplatesPath = $this->getPaths()['viewTemplatesPath'];
    $publicUrl = $this->getPaths()['publicUrl'];
    include $viewTemplatesPath . 'header.php';
?>

<section class="section-about">
    <h2 class="section-about__heading">Pasito Latino Dance</h2>
    <h2 class="section-about__subheading">Welcomes everyone who wants to enjoy dancing!</h2>
    <p class="section-about__text">Whether you are a beginner or an accomplished competitor, hard work, determination and lots of practice can make you a great dancer- join Flow Dance to make it happen!</p>
    <p class="section-about__text">Located near Oval station, and a stone's throw from Central London, Flow Dance boasts brand new and impressive <a href="#" class="section-about__text--link">modern studio facilities</a>. Owned and run by World Class International dance champions, Flow Dance and its <a href="#" class="section-about__text--link">top team</a> of professional dancers can guide you every step of the way through your dance journey.</p>
    <p class="section-about__text">We offer <a href="#" class="section-about__text--link">private lessons</a> and <a href="#" class="section-about__text--link">group classes</a> for both children and adults in a range of styles including: Ballroom &amp; Latin, Contemporary, Salsa, Competitors DanceSport Club, and more!</p>
    <p class="section-about__text"><a href="#" class="section-about__text--link">Book our dancers of all styles for your corporate event, private party, wedding animation, hen parties and stag dos, private functions, promotional events and media in London &amp; UK and worldwide. Wow your guests with an amazing dance show of your choice!</a></p>
    <iframe width="360" height="315" src="https://www.youtube.com/embed/U-JofUEsbD0" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
</section>

<section class="section-events">
    <h2 class="section-events__heading">Upcoming events</h2>
    <div class="section-events__single">
        <div class="section-events__single__poster">
            <figure>
                <img src="<?php echo $publicUrl; ?>img/event-poster.jpg">
            </figure>
        </div>
        <div class="section-events__single__details">
            <p class="section-events__single__details__text">Join us for Winter Blast edition at Flow Dance. Be prepared to dance the night away with great Ballroom and Latin songs accompained by live music. There will be pro-am and professional shows, cash bar and great vibes! Starting at 7.30pm with an Ice-Breaker Ballroom and Latin Class till late!</p>
            <button class="btn"><a href="#" class="btn__link">Buy your ticket now</a></button>
        </div>
    </div>
    <button class="btn"><a href="#" class="btn__link">See a list of past events</a></button>
</section>

<?php include $viewTemplatesPath . 'footer.php'; ?>