<?php

/** @var Controller $this */

$viewTemplatesPath = $this->getPaths()['viewTemplatesPath'];
$publicUrl = $this->getPaths()['publicUrl'];
?>

<!DOCTYPE html>
<html>
    <head>
        <title></title>
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,700" rel="stylesheet">
        <link rel="stylesheet" type="text/css" href="<?php echo $publicUrl; ?>css/font-awesome.min.css">
        <link rel="stylesheet" type="text/css" href="<?php echo $publicUrl; ?>css/style.css">
        <link rel="stylesheet" type="text/css" href="<?php echo $publicUrl; ?>css/navigation.css">
        <link rel="stylesheet" type="text/css" href="<?php echo $publicUrl; ?>css/error-messages.css">
    </head>
    <body>
        <header class="header">
            <nav class="header__nav">
                <div class="logo-box">
                    <a href="#"><img src="<?php echo $publicUrl; ?>img/logo-cropped.png"></a>
                </div>
                <?php include $viewTemplatesPath . 'navigation.php' ?>
            </nav>

            <button class="btn"><a href="#" class="btn__link">Book a tester class</a></button>
        </header>