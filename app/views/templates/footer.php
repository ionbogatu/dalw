<?php

/** @var Controller $this */

$publicUrl = $this->getPaths()['publicUrl'];

?>
        <section class="section-contact">
            <div class="section-contact__wrap">
                <h2 class="section-contact__heading">Contact us</h2>
                <img src="<?php echo $publicUrl; ?>img/logo-cropped.png" class="logo-contact">
                <p class="section-contact__text">Dance Flow Academy Ltd. <br>
                    Registered Office: 1-3 Brixton Rd., Kennington Business Park, Canterbury <br>Court Unit 3, SW9 6DE, London.<br>
                    Vat Registration Number: 184994839<br>
                    Registered in England. Established 2014.<br>
                </p>
                <ul class="section-contact__list">
                    <li class="section-contact__item">
                        <a href="tel:+40751234567" class="section-contact__link">
                            <i class="fa fa-phone" aria-hidden="true"></i> +40 75 12 34 567
                        </a>
                    </li>
                    <li class="section-contact__item">
                        <a href="mailto:support@pasito-latino-dance.ro" class="section-contact__link">
                            <i class="fa fa-envelope-o" aria-hidden="true"></i> support@pasito-latino-dance.ro
                        </a>
                    </li>
                </ul>
            </div>
        </section>

        <footer class="footer">
            <div class="logo-box">
                <a href="#"><img src="<?php echo $publicUrl; ?>img/logo-cropped.png"></a>
            </div>
            <div class="footer__copyright">
                <p class="footer__copyright__text">Copyright &copy; 2018</p>
                <p class="footer__copyright__owner">Pasito Latino Dance</p>
            </div>
        </footer>
        <script type="text/javascript" src="<?php echo $publicUrl; ?>js/jquery-3.3.1.js"></script>
        <script type="text/javascript" src="<?php echo $publicUrl; ?>js/custom.js"></script>
    </body>
</html>