<?php

/** @var Controller $this */
$publicUrl = $this->getPaths()['publicUrl'];
$currentPage = $this->getControllerAction();
/** @var Tutorial $tutorial */
$tutorial = $this->model('Tutorial');
$collection = $tutorial->getCollection();
$hasTutorialDropdown = (!empty($collection) && count($collection));
?>

<ul class="header__nav__list">
    <li class="header__nav__item <?php echo ($currentPage === 'home/index') ? 'active' : ''; ?>"><a class="header__nav__link" href="<?php echo $publicUrl; ?>"><span class="underline"></span>Home</a></li>
    <li class="header__nav__item <?php echo ($currentPage === 'about/index') ? 'active' : ''; ?>"><a href="<?php echo $publicUrl; ?>about/index" class="header__nav__link"><span class="underline"></span>About</a></li>
    <li class="header__nav__item <?php echo ($currentPage === 'tutorials/index') ? 'active' : ''; ?> <?php echo ($hasTutorialDropdown) ? 'header__nav-item__dropdown' : ''; ?>">
        <a href="<?php echo (!$hasTutorialDropdown) ? $publicUrl . 'tutorials/index' : '#'; ?>" class="header__nav__link"><span class="underline"></span>Tutorials</a>
        <?php if($hasTutorialDropdown){ ?>
            <ul class="header__nav__dropdown">
                <?php foreach($collection as $item){ ?>
                    <li><a class="header__nav__link" href="<?php echo $publicUrl; ?>tutorials/show/<?php echo $item['code']; ?>"><span class="underline"></span><?php echo $item['name']?></a></li>
                <?php } ?>
            </ul>
        <?php } ?>
    </li>
    <li class="header__nav__item <?php echo ($currentPage === 'events/index') ? 'active' : ''; ?>"><a href="<?php echo $publicUrl; ?>events/index" class="header__nav__link"><span class="underline"></span>Flow Events</a></li>
</ul>