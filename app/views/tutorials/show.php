<?php
/** @var \PathBuilder $pathBuilder */
$pathBuilder = $this->helper('PathBuilder');
$viewTemplatesPath = $pathBuilder->getViewTemplatesPath();
include $viewTemplatesPath . 'header.php';

/** @var Tutorial $tutorial*/
$tutorial = $this->model('Tutorial');
$collection = $tutorial->getCollection();

$courseExists = isset($data['id']) && isset($collection[$data['id']]) ? true : false;
?>

<section class="main" id="tutorial-content" style="margin-top: 0px;">
    <?php
        if(
            !$collection ||
            (
                $collection &&
                !count($collection)
            ) ||
            (
                $collection &&
                count($collection) &&
                !$courseExists
            )
        ): ?>
        <div class="main-content-wrapper">
            <div class="fw-page-builder-content">
                <div class="text-center glacier-section dark-color custom-bg" id="section-586bf3ab91879" style="">
                    <div class="parallax light-translucent-bg" style="background: url(&quot;https://flowdance-london.co.uk/wp-content/themes/everglades/images/patterns/wood-dark.png&quot;) top center repeat-x fixed;"></div>
                    <div class="container-fluid auto-height">
                        <div class="row">
                            <div class="col-xs-12 col-sm-12" style="background: transparent center center no-repeat fixed;padding-right: ;padding-bottom: 0;padding-left: ;margin-top: 0;margin-bottom: 0;">
                                <div class="heading-block text-center heading-block-line">
                                    <h1 class="special-heading"></h1>
                                    <p>&nbsp;</p>Latin Practice Nights <span>Every Monday &amp; Wednesday</span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div style="clear:both"></div>
                </div>
                <div class="separator- glacier-section" id="section-586bf3abbfb9f" style="padding-top: 45px; padding-bottom: 45px;">
                    <div class="container">
                        <section>
                            <div class="row">
                                <div class="col-xs-12 col-sm-4" style="background: transparent">
                                    <div class="feature-box text-center media-box">
                                        <div class="fbox-media"><img alt="Whole Venue - Ambience Lights" class="img-responsive" data-no-retina="" height="242" src="https://i0.wp.com/flowdance-london.co.uk/wp-content/uploads/2016/10/whole-studio4-10x24m-5760x3840.jpg?resize=5760%2C3840" width="363"></div>
                                        <div class="fbox-desc">
                                            <h2></h2><span class="subtitle"></span>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-8" style="background: transparent">
                                    <div class="text-block shortcode-container">
                                        <a class="dropdown">
                                            <p><!-- trigger button --><br></p></a><a href="#"><button>Learn Now</button></a>
                                        <p></p>
                                        <p><!-- dropdown menu --></p>
                                    </div>
                                </div>
                                <div class="text-block shortcode-container">
                                    <p>Flow Dance London's bi-weekly Latin Practice Night open to competitors of all levels. Join amateur and professional Latin couples, Pro-Am and same-sex couples on the floor getting ready for your next competition. At Flow Dance community we hope to create a supportive and inspirational environment for dancers to share their passion for dance. 5 dances will be played: Rumba, Chacha, Samba, Paso Doble and Jive</p>
                                    <p><b>Studio amenities:</b></p>
                                    <ul>
                                        <li>Air conditioning</li>
                                        <li>Atmospheric lighting</li>
                                        <li>Competitive tracks</li>
                                        <li>New, wood-sprung flooring</li>
                                        <li>Lounge and Bar</li>
                                        <li>Showers</li>
                                    </ul>
                                    <p>Practice sessions will be held on Mondays and Wednesdays from 8:30-10 PM. £5 per person or get our Monthly Practice Membership for £50 per person!</p>
                                    <p>You don't need to come with your dance partner if you fancy a Practice Night on your own!</p>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12 col-sm-4" style="background: transparent">
                                    <div class="feature-box text-center media-box">
                                        <div class="fbox-media"><img alt="Whole Venue - Ambience Lights" class="img-responsive" data-no-retina="" height="242" src="https://i0.wp.com/flowdance-london.co.uk/wp-content/uploads/2016/10/whole-studio4-10x24m-5760x3840.jpg?resize=5760%2C3840" width="363"></div>
                                        <div class="fbox-desc">
                                            <h2></h2><span class="subtitle"></span>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-8" style="background: transparent">
                                    <div class="text-block shortcode-container">
                                        <a class="dropdown">
                                            <p><!-- trigger button --><br></p></a><a href="#"><button>Learn Now</button></a>
                                        <p></p>
                                        <p><!-- dropdown menu --></p>
                                    </div>
                                </div>
                                <div class="text-block shortcode-container">
                                    <p>Flow Dance London's bi-weekly Latin Practice Night open to competitors of all levels. Join amateur and professional Latin couples, Pro-Am and same-sex couples on the floor getting ready for your next competition. At Flow Dance community we hope to create a supportive and inspirational environment for dancers to share their passion for dance. 5 dances will be played: Rumba, Chacha, Samba, Paso Doble and Jive</p>
                                    <p><b>Studio amenities:</b></p>
                                    <ul>
                                        <li>Air conditioning</li>
                                        <li>Atmospheric lighting</li>
                                        <li>Competitive tracks</li>
                                        <li>New, wood-sprung flooring</li>
                                        <li>Lounge and Bar</li>
                                        <li>Showers</li>
                                    </ul>
                                    <p>Practice sessions will be held on Mondays and Wednesdays from 8:30-10 PM. £5 per person or get our Monthly Practice Membership for £50 per person!</p>
                                    <p>You don't need to come with your dance partner if you fancy a Practice Night on your own!</p>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12 col-sm-4" style="background: transparent">
                                    <div class="feature-box text-center media-box">
                                        <div class="fbox-media"><img alt="Whole Venue - Ambience Lights" class="img-responsive" data-no-retina="" height="242" src="https://i0.wp.com/flowdance-london.co.uk/wp-content/uploads/2016/10/whole-studio4-10x24m-5760x3840.jpg?resize=5760%2C3840" width="363"></div>
                                        <div class="fbox-desc">
                                            <h2></h2><span class="subtitle"></span>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-8" style="background: transparent">
                                    <div class="text-block shortcode-container">
                                        <a class="dropdown">
                                            <p><!-- trigger button --><br></p></a><a href="#"><button>Learn Now</button></a>
                                        <p></p>
                                        <p><!-- dropdown menu --></p>
                                    </div>
                                </div>
                                <div class="text-block shortcode-container">
                                    <p>Flow Dance London's bi-weekly Latin Practice Night open to competitors of all levels. Join amateur and professional Latin couples, Pro-Am and same-sex couples on the floor getting ready for your next competition. At Flow Dance community we hope to create a supportive and inspirational environment for dancers to share their passion for dance. 5 dances will be played: Rumba, Chacha, Samba, Paso Doble and Jive</p>
                                    <p><b>Studio amenities:</b></p>
                                    <ul>
                                        <li>Air conditioning</li>
                                        <li>Atmospheric lighting</li>
                                        <li>Competitive tracks</li>
                                        <li>New, wood-sprung flooring</li>
                                        <li>Lounge and Bar</li>
                                        <li>Showers</li>
                                    </ul>
                                    <p>Practice sessions will be held on Mondays and Wednesdays from 8:30-10 PM. £5 per person or get our Monthly Practice Membership for £50 per person!</p>
                                    <p>You don't need to come with your dance partner if you fancy a Practice Night on your own!</p>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12 col-sm-4" style="background: transparent">
                                    <div class="feature-box text-center media-box">
                                        <div class="fbox-media"><img alt="Whole Venue - Ambience Lights" class="img-responsive" data-no-retina="" height="242" src="https://i0.wp.com/flowdance-london.co.uk/wp-content/uploads/2016/10/whole-studio4-10x24m-5760x3840.jpg?resize=5760%2C3840" width="363"></div>
                                        <div class="fbox-desc">
                                            <h2></h2><span class="subtitle"></span>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-8" style="background: transparent">
                                    <div class="text-block shortcode-container">
                                        <a class="dropdown">
                                            <p><!-- trigger button --><br></p></a><a href="#"><button>Learn Now</button></a>
                                        <p></p>
                                        <p><!-- dropdown menu --></p>
                                    </div>
                                </div>
                                <div class="text-block shortcode-container">
                                    <p>Flow Dance London's bi-weekly Latin Practice Night open to competitors of all levels. Join amateur and professional Latin couples, Pro-Am and same-sex couples on the floor getting ready for your next competition. At Flow Dance community we hope to create a supportive and inspirational environment for dancers to share their passion for dance. 5 dances will be played: Rumba, Chacha, Samba, Paso Doble and Jive</p>
                                    <p><b>Studio amenities:</b></p>
                                    <ul>
                                        <li>Air conditioning</li>
                                        <li>Atmospheric lighting</li>
                                        <li>Competitive tracks</li>
                                        <li>New, wood-sprung flooring</li>
                                        <li>Lounge and Bar</li>
                                        <li>Showers</li>
                                    </ul>
                                    <p>Practice sessions will be held on Mondays and Wednesdays from 8:30-10 PM. £5 per person or get our Monthly Practice Membership for £50 per person!</p>
                                    <p>You don't need to come with your dance partner if you fancy a Practice Night on your own!</p>
                                </div>
                            </div>
                        </section>
                    </div>
                </div>
            </div>
        </div>
    <?php else: ?>
        <?php $course = $collection[$data['id']]; ?>
        <h2 class="course-title"><?php echo $course['name']; ?></h2>
        <p class="course-description"><?php echo $course['description']; ?></p>
        <div style="display: flex;">
            <div class="video">

            </div><div class="right-bar">
                <ul class="contents">
                    <?php foreach($course['resources'] as $resource): ?>
                            <li class="content-item">
                            <div class="content-item-icon">
                                <?php if($resource['type'] === 'article'): ?>
                                    <i class="fa fa-file-text-o" aria-hidden="true"></i>
                                <?php elseif($resource['type'] === 'video'): ?>
                                    <i class="fa fa-play-circle" aria-hidden="true"></i>
                                <?php elseif($resource['type'] === 'tutorial'): ?>
                                    <i class="fa fa-diamond" aria-hidden="true"></i>
                                <?php endif; ?>
                            </div>
                            <div class="content-item-name">
                                <h4><?php echo $resource['name']; ?></h4>
                                <p><?php echo $resource['description']; ?></p>
                            </div>
                        </li>
                    <?php endforeach; ?>
                </ul>
            </div>
        </div>
    <?php endif; ?>
</section>

<?php include $viewTemplatesPath . 'footer.php'; ?>