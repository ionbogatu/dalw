<?php
    /** @var \PathBuilder $pathBuilder */
    $pathBuilder = $this->helper('PathBuilder');
    $viewTemplatesPath = $pathBuilder->getViewTemplatesPath();
    include $viewTemplatesPath . 'header.php';
?>

<section class="main" style="margin-top: 0px;">
    <div class="main-content-wrapper">
        <div class="fw-page-builder-content">
            <div class="glacier-section" id="home-events" style="padding-top: 2px; padding-bottom: 2px; margin-bottom: 15px; ; background-color: #004c70">
                <div class="container">
                    <section>
                        <div class="row">
                            <div class="col-xs-12 col-sm-12" style="background: #004c70 top left no-repeat scroll;">
                                <div class="text-block shortcode-container">
                                    <h2 style="text-align: center;">UPCOMING EVENTS</h2>
                                </div>
                            </div>
                        </div>
                    </section>
                </div>
            </div>
            <div class="separator- glacier-section" id="section-5734c762333f0" style="padding-top: 45px; padding-bottom: 45px;">
                <div class="container">
                    <section>
                        <div class="row">
                            <div class="col-xs-12 col-sm-6" style="background: transparent"><img alt="website-pic_christmas-2017" class="img-responsive align-center" data-no-retina="" height="345" src="https://i1.wp.com/flowdance-london.co.uk/wp-content/uploads/2016/05/website-pic_christmas-2017.png?resize=642%2C392&amp;ssl=1" width="565"></div>
                            <div class="col-xs-12 col-sm-6" style="background: transparent"><img alt="website-pic_autumn-2017" class="img-responsive align-center" data-no-retina="" height="345" src="https://i0.wp.com/flowdance-london.co.uk/wp-content/uploads/2015/02/website-pic_autumn-2017.png?resize=642%2C392&amp;ssl=1" width="565"></div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12 col-sm-6" style="background: transparent"><img alt="website-pic_summer-2017" class="img-responsive align-center" data-no-retina="" height="345" src="https://i0.wp.com/flowdance-london.co.uk/wp-content/uploads/2015/02/website-pic_summer-2017.png?resize=1338%2C817&amp;ssl=1" width="565"></div>
                            <div class="col-xs-12 col-sm-6" style="background: transparent"><img alt="website-pic_anniversary" class="img-responsive align-center" data-no-retina="" height="345" src="https://i2.wp.com/flowdance-london.co.uk/wp-content/uploads/2015/02/website-pic_anniversary-642x392.png?resize=642%2C392" width="565"></div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12 col-sm-6" style="background: transparent">
                                <div class="video-wrapper shortcode-container">
                                    <span class="embed-youtube" style="text-align:center; display: block;"><iframe allowfullscreen="true" class="youtube-player" height="390" src="https://www.youtube.com/embed/bmfdKI0Zmao?version=3&amp;rel=1&amp;fs=1&amp;autohide=2&amp;showsearch=0&amp;showinfo=1&amp;iv_load_policy=1&amp;wmode=transparent" style="border:0;" width="640"><span class="embed-youtube" style="text-align:center; display: block;"></span></iframe></span>
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-6 col-md-3" style="background: transparent"><img alt="christmas-esa-2016-1-64-flow-dance-event-image" class="img-responsive align-center" data-no-retina="" height="163" src="https://i0.wp.com/flowdance-london.co.uk/wp-content/uploads/2016/05/Christmas-ESA-2016-1.64-Flow-Dance-event-image-e1479324777194-642x392.jpg?resize=642%2C392" width="268"> <img alt="digital_video-pics[autumn-blast][3]" class="img-responsive align-center" data-no-retina="" height="163" src="https://i2.wp.com/flowdance-london.co.uk/wp-content/uploads/2016/05/digital_video-picsautumn-blast3-642x392.png?resize=642%2C392" width="268"></div>
                            <div class="col-xs-12 col-sm-6 col-md-3" style="background: transparent"><img alt="digital_video-pics[latin-flow][2]" class="img-responsive align-center" data-no-retina="" height="163" src="https://i0.wp.com/flowdance-london.co.uk/wp-content/uploads/2016/05/digital_video-picslatin-flow2-642x392.png?resize=642%2C392" width="268"> <img alt="Autumn ESA! 2016-Flow Dance web site-642x392" class="img-responsive align-center" data-no-retina="" height="163" src="https://i2.wp.com/flowdance-london.co.uk/wp-content/uploads/2016/05/Autumn-ESA-2016-Flow-Dance-web-site-642x392-1-642x392.png?resize=642%2C392" width="268"></div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12 col-sm-6" style="background: transparent">
                                <div class="video-wrapper shortcode-container">
                                    <span class="embed-youtube" style="text-align:center; display: block;"><iframe allowfullscreen="true" class="youtube-player" height="390" src="https://www.youtube.com/embed/i7QwI1DfM18?version=3&amp;rel=1&amp;fs=1&amp;autohide=2&amp;showsearch=0&amp;showinfo=1&amp;iv_load_policy=1&amp;wmode=transparent" style="border:0;" width="640"><span class="embed-youtube" style="text-align:center; display: block;"></span></iframe></span>
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-6 col-md-3" style="background: transparent"><img alt="Tango Summer Ball" class="img-responsive align-center" data-no-retina="" height="163" src="https://i0.wp.com/www.flowdance-london.co.uk/wp-content/uploads/2016/05/digital_video-picstango-642x392.jpeg?resize=642%2C392" width="268"> <img alt="digital_video-pics[kids]" class="img-responsive align-center" data-no-retina="" height="163" src="https://i1.wp.com/www.flowdance-london.co.uk/wp-content/uploads/2016/03/digital_video-picskids-642x392.png?resize=642%2C392" width="268"></div>
                            <div class="col-xs-12 col-sm-6 col-md-3" style="background: transparent"><img alt="digital_video-pics[house]" class="img-responsive align-center" data-no-retina="" height="163" src="https://i0.wp.com/www.flowdance-london.co.uk/wp-content/uploads/2016/05/digital_video-picshouse-642x392.png?resize=642%2C392" width="268"> <img alt="digital_video-pics[killick]" class="img-responsive align-center" data-no-retina="" height="163" src="https://i2.wp.com/www.flowdance-london.co.uk/wp-content/uploads/2016/05/digital_video-picskillick-642x392.png?resize=642%2C392" width="268"></div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12 col-sm-6" style="background: transparent">
                                <div class="video-wrapper shortcode-container">
                                    <span class="embed-youtube" style="text-align:center; display: block;"><iframe allowfullscreen="true" class="youtube-player" height="390" src="https://www.youtube.com/embed/aqQHsvReJII?version=3&amp;rel=1&amp;fs=1&amp;autohide=2&amp;showsearch=0&amp;showinfo=1&amp;iv_load_policy=1&amp;wmode=transparent" style="border:0;" width="640"><span class="embed-youtube" style="text-align:center; display: block;"></span></iframe></span>
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-6" style="background: transparent">
                                <div class="video-wrapper shortcode-container">
                                    <span class="embed-youtube" style="text-align:center; display: block;"><iframe allowfullscreen="true" class="youtube-player" height="390" src="https://www.youtube.com/embed/DLgc1aMfvg8?version=3&amp;rel=1&amp;fs=1&amp;autohide=2&amp;showsearch=0&amp;showinfo=1&amp;iv_load_policy=1&amp;wmode=transparent" style="border:0;" width="640"><span class="embed-youtube" style="text-align:center; display: block;"></span></iframe></span>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12 col-sm-6" style="background: transparent">
                                <div class="video-wrapper shortcode-container">
                                    <span class="embed-youtube" style="text-align:center; display: block;"><iframe allowfullscreen="true" class="youtube-player" height="390" src="https://www.youtube.com/embed/pUFARb7IEKc?version=3&amp;rel=1&amp;fs=1&amp;autohide=2&amp;showsearch=0&amp;showinfo=1&amp;iv_load_policy=1&amp;wmode=transparent" style="border:0;" width="640"><span class="embed-youtube" style="text-align:center; display: block;"></span></iframe></span>
                                </div>
                            </div>
                        </div>
                    </section>
                </div>
            </div>
            <style type="text/css">
                #section-56e147072ff82 .special-heading, #section-56e147072ff82 blockquote { color : #ffffff; } padding-left:5px;
            </style>
            <div class="text-left glacier-section" id="section-56e147072ff82" style="padding-top: 5px; padding-bottom: 5px; margin-top: 15px; ; color: #ffffff; background-color: #004c70">
                <div class="container-fluid">
                    <section>
                        <div class="row">
                            <div class="col-xs-12 col-sm-12" style="background: transparent center center no-repeat fixed;">
                                <div class="text-block shortcode-container">
                                    <div class="contained"></div>
                                </div>
                            </div>
                        </div>
                    </section>
                </div>
            </div>
        </div>
    </div>
</section>

<?php include $viewTemplatesPath . 'footer.php'; ?>