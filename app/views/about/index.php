<?php
    /** @var \PathBuilder $pathBuilder */
    $pathBuilder = $this->helper('PathBuilder');
    $viewTemplatesPath = $pathBuilder->getViewTemplatesPath();
    include $viewTemplatesPath . 'header.php';
?>
<section class="main" style="margin-top: 0px;">
    <div class="main-content-wrapper">
        <div class="fw-page-builder-content">
            <div class="glacier-section" id="section-57fa438b3927a" style="padding-top: 2px; padding-bottom: 2px; margin-bottom: 15px; ; background-color: #004c70">
                <div class="container">
                    <section>
                        <div class="row">
                            <div class="col-xs-12 col-sm-12" style="background: transparent">
                                <div class="text-block shortcode-container">
                                    <h2 style="text-align: center;">ABOUT</h2>
                                </div>
                            </div>
                        </div>
                    </section>
                </div>
            </div>
            <div class="separator- glacier-section" id="section-57fa4415daf0f" style="padding-top: 45px; padding-bottom: 45px;">
                <div class="container">
                    <section>
                        <div class="row">
                            <div class="col-xs-12 col-sm-6" style="background: transparent">
                                <div class="heading-block text-left heading-block-line">
                                    <h1 class="special-heading">Flow Dance London</h1><span>State of the art studio space available for hire</span>
                                </div>
                                <div class="text-block shortcode-container">
                                    <p>Flow Dance London offers a great space for dancers to unite. We have:</p>
                                    <ul>
                                        <li>2440 sq ft for the whole space</li>
                                        <li>Wooden sprung dance floor</li>
                                        <li>Professional, personalised sound system</li>
                                        <li>Controlled air conditioning</li>
                                        <li>Dimming lights to suit your mood</li>
                                        <li>Party lights in different shades</li>
                                        <li>Mirrors &amp; barres</li>
                                        <li>Sound-proofed partition walls</li>
                                        <li>Toilet facilities</li>
                                        <li>Showers and changing rooms</li>
                                        <li>Comfortable lounge</li>
                                        <li>Pay-by-phone parking on site</li>
                                        <li>Free parking on small streets around</li>
                                        <li>Restaurants and parks near-by</li>
                                        <li>Walking distance from Oval station</li>
                                    </ul>
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-6" style="background: transparent">
                                <div class="owl-carousel slideshow owl-theme" id="owl-carousel-5a6da406930cb" style="opacity: 1; display: block;">
                                    <div class="owl-wrapper-outer">
                                        <div class="owl-wrapper" style="width: 16950px; left: 0px; display: block;">
                                            <div class="owl-item" style="width: 565px;">
                                                <div class="owl-slideshow-item">
                                                    <img alt="Studio 1 - Half" data-no-retina="" height="377" src="<?php echo $publicUrl; ?>about_files/studio1-10x8m-576x384.jpg" width="565"> <span class="caption">Studio 1 - Half</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                </div>
            </div>
            <style type="text/css">
                #section-57fa438b5ec03 .special-heading, #section-57fa438b5ec03 blockquote { color : #ffffff; }
            </style>
            <div class="glacier-section" id="section-57fa438b5ec03" style="padding-top: 5px; padding-bottom: 5px; margin-top: 15px; ; color: #ffffff; background-color: #004c70">
                <div class="container-fluid">
                    <section>
                        <div class="row">
                            <div class="col-xs-12 col-sm-12" style="background: transparent">
                                <div class="text-block shortcode-container" style="font-size: ;"></div>
                            </div>
                        </div>
                    </section>
                </div>
            </div>
        </div>
    </div>
</section>

<?php include $viewTemplatesPath . 'footer.php'; ?>