<?php
/**
 * Created by PhpStorm.
 * User: JohnRich
 * Date: 1/27/2018
 * Time: 1:11 PM
 */

class Controller
{
    protected $controllerName;

    protected $actionName;

    protected $paths = [];

    public function __construct($controllerName, $actionName)
    {
        $this->controllerName = $controllerName;
        $this->actionName = $actionName;

        $urlBuilder = $this->helper('UrlBuilder');
        $pathBuilder = $this->helper('PathBuilder');

        $this->paths = [
            'publicUrl' => $urlBuilder->getPublicUrl(),
            'viewTemplatesPath' => $pathBuilder->getViewTemplatesPath()
        ];
    }

    public function index(){

        echo '';
        exit;
    }

    public function model($model){

        require_once '../app/models/' . $model . '.php';
        return new $model();
    }

    public function view($view, $data = [])
    {
        if(file_exists('../app/views/' . $view . '.php')){
            require_once '../app/views/' . $view . '.php';
        }else{
            require_once '../app/views/e404/index.php';
        }
    }

    public function helper($helper){

        require_once '../app/helpers/' . $helper . '.php';
        return new $helper();
    }

    public function getControllerAction()
    {
        return $this->controllerName . '/' . $this->actionName;
    }

    public function getPaths()
    {
        return $this->paths;
    }
}