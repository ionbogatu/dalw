<?php
/**
 * Created by PhpStorm.
 * User: JohnRich
 * Date: 1/27/2018
 * Time: 1:08 PM
 */

require_once '../vendor/autoload.php';
require_once 'core/App.php';
require_once 'core/Controller.php';

$app = new App();