<?php
/**
 * Created by PhpStorm.
 * User: JohnRich
 * Date: 1/28/2018
 * Time: 10:50 AM
 */

require_once 'FileSystem.php';

class UrlBuilder extends FileSystem
{
    public function getPublicUrl(){

        return '/' . self::PROJECT_NAME . '/public/';
    }
}