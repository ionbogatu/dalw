<?php
/**
 * Created by PhpStorm.
 * User: JohnRich
 * Date: 1/28/2018
 * Time: 10:50 AM
 */

require_once 'FileSystem.php';

class PathBuilder extends FileSystem
{
    public function getViewTemplatesPath()
    {
        return $_SERVER['DOCUMENT_ROOT'] . '/' . self::PROJECT_NAME . '/app/views/templates/';
    }

    public function getTutorialsPath(){
        return $_SERVER['DOCUMENT_ROOT'] . '/' . self::PROJECT_NAME . '/public/tutorials/';
    }
}