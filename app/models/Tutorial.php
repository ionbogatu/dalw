<?php
/**
 * Created by PhpStorm.
 * User: JohnRich
 * Date: 1/28/2018
 * Time: 3:29 PM
 */

class Tutorial
{
    protected $_collection = null;

    public function getCollection(){

        if(!empty($this->_collection)){

            return $this->_collection;
        }else{

            $pathBuilder = new PathBuilder();
            $tutorialsPath = $pathBuilder->getTutorialsPath();

            $this->_collection = $this->getTutorialsConfig($tutorialsPath);
            return $this->_collection;
        }
    }

    private function getTutorialsConfig($path){

        $configFiles = glob($path . '/*/config.json', GLOB_NOSORT & GLOB_ONLYDIR);
        $result = [];
        foreach($configFiles as $file){
            $json = json_decode('[' . file_get_contents($file) . ']', true)[0];

            if($json){
                $result[$json['code']] = $json;
            }
        }

        return $result;
    }
}